import React from "react";
import { Image, Text, View } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";

//contents of the slide
const slides = [
    {
    key: "one",
    title: "WHY I CHOSE IT COURSE?", 
    text: 
    "Always in demand",
    image: require("./picture/img1.png"),
    },
    {
    key: "two",
    text:
    "You'll earn good money",
    image: require("./picture/img2.png"), 
    },
    {
    key: "three",
    text:
    "Flexible work style",
    image: require("./picture/img3.png"),
    },
    {
    key: "four",
    text:
    "You can what you're studying",
    image: require("./picture/img4.png"),
    },
    {
    key: "five",
    text:
    "Variety of career choices",
    image: require("./picture/img5.png"),
    },
    {
    key: "six",
    text:
    "I love digital works",
    image: require("./picture/img6.png"),
    },
]

const Slide = ({ item }) => {
    return (
    <View style={{ flex: 1, backgroundColor: 'white'}}>
        <Image source={item.image}
        style={{
            resizeMode: 'contain',
            height: "70%",
            width: "100%",
        }}
        />
        <Text style={{
            paddingTop: 5,
            paddingBottom: 5,
            fontSize: 20,
            fontWeight: "bold",
            color: "#808080",
            alignSelf: "center",
        }}
        >
        {item.title}
        </Text>

        <Text style={{
        textAlign:"center",
        color:"#000000",
        fontSize:15,
        paddingHorizontal:20,
        }}>
        {item.text}
        </Text>
    </View>
    )
    };

    export default function IntroSliders({navigation}) {
    return(
        <AppIntroSlider
        renderItem={({item}) => <Slide item={item}/> } 
        data={slides} 
        activeDotStyle={{
            backgroundColor:"#5f9ea0",
            width:10
        }}
        onDone={() => navigation.push('Intro')}
        showDoneButton={true}
        renderDoneButton={()=> <Text style={{color: '#696969', fontWeight: 'bold', margin: 1, fontSize: 15}}>
            Home</Text>}
        showNextButton={true}
        renderNextButton={()=> <Text style={{color: '#696969', fontWeight: 'bold', margin: 1, fontSize: 15}}>
            Continue</Text>}
        /> 
    ) 
};