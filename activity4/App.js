import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// Screens
import Wenou from './Wenou.js';

const Stack = createNativeStackNavigator(); 

export default function App () {

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
          <Stack.Screen name= 'Intro' component={Wenou} />
        </Stack.Navigator>
    </NavigationContainer>
  )
}
